package com.developia.task.bootstrap;

import com.developia.task.model.Course;
import com.developia.task.model.Groups;
import com.developia.task.model.Tutor;
import com.developia.task.repository.CourseRepository;
import com.developia.task.repository.GroupRepository;
import com.developia.task.repository.TutorRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.xml.transform.sax.SAXSource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {
    private final GroupRepository groupRepository;
    private final CourseRepository courseRepository;

    public DataLoader(GroupRepository groupRepository, CourseRepository courseRepository) {
        this.groupRepository = groupRepository;
        this.courseRepository = courseRepository;

    }

    @Override
    public void run(String... args) throws Exception {
        Course course = new Course();

        course.setName("Summer");

        Groups groups1 = new Groups();
        groups1.setName("Tk");
        course.addGroup(groups1);
        Groups groups2 = new Groups();
        groups2.setName("Tk-2");
        course.addGroup(groups2);

        courseRepository.save(course);

//Course c1=  courseRepository.findById(1).get();
//        System.out.println(c1.getGroups());


    }
}
