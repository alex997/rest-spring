package com.developia.task.mapstruct;

import java.sql.Date;
import java.util.List;

public class GroupDTO {
    private Integer id;
    private TutorDTO tutor;
    private CourseDTO course;
    private List<StudentDTO> students;
    private Date startDate;
    private Date endDate;
}
