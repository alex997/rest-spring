package com.developia.task.mapstruct;

import java.sql.Date;

public class TutorDTO {
    private Integer id;
    private String name;
    private String surname;
    private String phone;
    private Date birthday;
}
