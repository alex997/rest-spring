package com.developia.task.controller;

import com.developia.task.model.Groups;
import com.developia.task.services.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GroupController {
    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }
    @GetMapping("/groups")
    @ResponseStatus(HttpStatus.OK)
    public List<Groups> getAllGroups() {
          return groupService.getGroups();
    }
    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addGroup(@RequestBody Groups groups) {
        groupService.save(groups);
    }
    @PostMapping("/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateGroup(@PathVariable Integer id,@RequestBody Groups groups) {
        groupService.update(id,groups);
    }
}
