package com.developia.task.services.impl;

import com.developia.task.model.Groups;
import com.developia.task.repository.GroupRepository;
import com.developia.task.services.GroupService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {
    private  final GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public List<Groups> getGroups() {
        return groupRepository.findAll();
    }

    @Override
    public void save(Groups group) {
        groupRepository.save(group);
    }

    @Override
    public Groups update(Integer id, Groups group) {
        group.setId(id);
        return groupRepository.save(group);
    }
}
