package com.developia.task.services.impl;

import com.developia.task.model.Course;
import com.developia.task.repository.CourseRepository;
import com.developia.task.services.CourseService;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getAllCourses() {
//       List<Course> courses = StreamSupport.stream(courseRepository.findAll().spliterator(),false).collect(Collectors.toList());

        return courseRepository.findAll();
    }
}
