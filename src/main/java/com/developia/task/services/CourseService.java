package com.developia.task.services;

import com.developia.task.model.Course;

import java.util.List;

public interface CourseService {
    List<Course> getAllCourses();
}
