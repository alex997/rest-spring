package com.developia.task.services;

import com.developia.task.model.Groups;

import java.util.List;

public interface GroupService {
    List<Groups> getGroups();
    void save(Groups group);
    Groups update(Integer id, Groups group);
}
