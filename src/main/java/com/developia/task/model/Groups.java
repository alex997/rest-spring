package com.developia.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
@Entity
@Data
public class Groups {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    @JsonIgnore
    private Course course;




//    @ManyToOne
//    @JoinColumn(name = "tutor_id")
//    private Tutor tutor;
//
//    private Date startDate;
//    private Date endDate;
//    @ManyToMany
//    @JoinTable(name = "student_group",joinColumns = @JoinColumn(name = "group_id")
//            ,inverseJoinColumns = @JoinColumn(name = "student_id"))
//    private List<Student> students;


}