package com.developia.task.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String surname;
    private String phone;
    private Date birthday;
//    @ManyToMany
//    @JoinTable(name = "student_group",joinColumns = @JoinColumn(name = "student_id")
//            ,inverseJoinColumns = @JoinColumn(name = "group_id"))
//    List<Groups> groups;


}
