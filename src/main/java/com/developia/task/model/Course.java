package com.developia.task.model;

import com.developia.task.repository.GroupRepository;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "course",fetch = FetchType.LAZY)
    private List<Groups> groups;


    public void addGroup(Groups g){
        if(groups==null){
            groups=new ArrayList<>();

        }
        g.setCourse(this);
        groups.add(g);

    }


}
